<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('insights');
});
// Route::get('insight',"Insights@list");
Route::post('insights','Insights@index');
// Route::get('download','Insights@downloadJSON')->name('download_movies');
