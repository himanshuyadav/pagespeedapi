<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;

class Insights extends Controller
{
    //
    // private $url;
    protected $startTime;
    // public function __construct() {
    //     $this->startTime = microtime(true);
    // }
    public function index(Request $request){
        // $this->url=$request->input('url');
        $url =$request->input('url');
        // echo   "Entered Url : " .$url;s
        // $this->list($url);
        $api_key="AIzaSyBtRCipjXInEt6z0LEnU4r5_o8lC0rYRDU ";
        // $responseTime = microtime(true) - LARAVEL_START; ///Server Response Time   
        $time_start = microtime(true); // Total Execution time

        $data = Http::get("https://www.googleapis.com/pagespeedonline/v5/runPagespeed?url=".$url."&key=".$api_key);
        $time_end = microtime(true);

        $execution_time = ($time_end - $time_start);
        
        
        //  Response Details into a file:::


        $filename = "response.json";
        $handle = fopen($filename, 'w+');
        fwrite($handle, "'Api Response Time(in Seconds) : '$execution_time '\n';$data");
        fclose($handle);
        $headers = array('Content-type'=> 'application/json');
     
        // echo '<b>Total Execution Time:</b> '.$execution_time.' Seconds';
        // dd($execution_time);
        return response()->download($filename,'response.json',$headers);
    }

    // public function  list(){
    //     $api_key="AIzaSyBtRCipjXInEt6z0LEnU4r5_o8lC0rYRDU ";
    //     // $respone=Http::get("https://www.googleapis.com/pagespeedonline/v5/runPagespeed?url=https://www.flipkart.com&key=AIzaSyBtRCipjXInEt6z0LEnU4r5_o8lC0rYRDU");
    //     // dd($respone->headers());
    //     // $url =$this->url;
    //     dd($url);
    //     // echo $this->url;
        
        
    // }
    // public function downloadJSON(Request $request){
    //     $filename = "response.json";
    //     $handle = fopen($filename, 'w+');
    //     fwrite($handle, $data);
    //     fclose($handle);
    //     $headers = array('Content-type'=> 'application/json');
    //     return response()->download($filename,'response.json',$headers);
   }

