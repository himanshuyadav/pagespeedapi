<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>
    <div class="container-fluid pt-3">
    <div class="form-group row mt-3">
    <form role="form" action="{{url('insights')}}" method="POST">
        @csrf
        <label for="example-url-input" class="col-2 col-form-label ">URL</label>
        <div class="col-10">
          <input class="form-control mr-3" name="url" type="url" value="" id="url" placeholder="Url">
        </div>
        <input class="btn btn-primary ml-3" type="submit" value="Submit">
    </form>
    </div>
</body>
</html>